
-- Copyright (C) 2020-2022 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local SchallAlienTech = false
local EnhancedGridEnergyStores = false

local function load_settings()
	SchallAlienTech = script.active_mods['SchallAlienTech'] ~= nil and settings.startup['k2c-schall-alien-tech'].value and settings.startup['alientech-satellite-enable'].value
	EnhancedGridEnergyStores = script.active_mods['EnhancedGridEnergyStores'] ~= nil and settings.startup['k2c-e-g-e-s'].value
end

script.on_load(load_settings)
script.on_configuration_changed(load_settings)
script.on_init(load_settings)

if settings.startup['k2c-space-exploration'].value and script.active_mods['space-exploration'] and not settings.startup['k2c-space-exploration-remove-satellite'].value then
	script.on_event(defines.events.on_rocket_launched, function(event)
		if event.rocket and event.rocket.valid then
			if SchallAlienTech then
				if event.rocket.get_item_count("Schall-satellite-astro-2") > 0 then
					remote.call("space-exploration", "launch_satellite", {force_name = event.rocket.force.name, surface = event.rocket.surface, count = 2})
				end

				if event.rocket.get_item_count("Schall-satellite-bio-1") > 0 then
					remote.call("space-exploration", "launch_satellite", {force_name = event.rocket.force.name, surface = event.rocket.surface, count = 1})
				end

				if event.rocket.get_item_count("Schall-satellite-bio-2") > 0 then
					remote.call("space-exploration", "launch_satellite", {force_name = event.rocket.force.name, surface = event.rocket.surface, count = 2})
				end
			end

			if EnhancedGridEnergyStores then
				if event.rocket.get_item_count("nimh-satellite") > 0 then
					remote.call("space-exploration", "launch_satellite", {force_name = event.rocket.force.name, surface = event.rocket.surface, count = 2})
				end

				if event.rocket.get_item_count("li-ion-satellite") > 0 then
					remote.call("space-exploration", "launch_satellite", {force_name = event.rocket.force.name, surface = event.rocket.surface, count = 4})
				end

				if event.rocket.get_item_count("flywheel-satellite") > 0 then
					remote.call("space-exploration", "launch_satellite", {force_name = event.rocket.force.name, surface = event.rocket.surface, count = 8})
				end
			end
		end
	end)
end
